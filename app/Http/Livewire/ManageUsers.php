<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\Request;

class ManageUsers extends Component
{
    public $showdeleteUserModal = false;

    public $currentUser;

    public function mount(){
        $this->currentUser = new User();
    }

    public function confirmDelete(User $user)
    {
        $this->currentUser = $user;
        $this->showdeleteUserModal = true;
    }

    public function deleteUser(User $user)
    {
        $this->currentUser->delete();
        $this->showdeleteUserModal = false;
    }

    public function render()
    {
        return view('livewire.manage-users')->with('users',User::all());
    }
}
