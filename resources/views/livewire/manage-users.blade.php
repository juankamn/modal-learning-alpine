<div class="flex flex-col">
    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table class="min-w-full divide-y divide-gray-200">
                    <thead class="bg-gray-50">
                    <tr>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Name
                        </th>
                        <th scope="col" class="relative px-6 py-3">
                            <span class="sr-only">Delete</span>
                        </th>
                    </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                    @foreach($users as $user)
                        <tr>
                            <td class="px-6 py-4 whitespace-nowrap">
                                <div class="flex items-center">
                                    <div>
                                        <div class="text-sm font-medium text-gray-900">
                                            {{$user->name}}
                                        </div>
                                        <div class="text-sm text-gray-500">
                                            {{$user->email }}
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                <button class="text-indigo-600 hover:text-indigo-900" wire:click="confirmDelete({{$user->id}})">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                    <!-- More people... -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <form wire:submit.prevent="deleteUser">
        <x-confirmation-modal wire:model.defer="showdeleteUserModal">
            <x-slot name="title">
                Delete {{ $currentUser->name }}
            </x-slot>

            <x-slot name="body">
                Please describe your question/issue in detail below:
            </x-slot>

            <x-slot name="footer">
                {{--            <a href="#"--}}
                {{--               class="bg-gray-400 hover:bg-gray-500 text-xs uppercase py-2 px-4 rounded-md text-white transition-all duration-200">Cancel</a>--}}
                {{--            <x-button class="bg-gray-400 hover:bg-gray-500">Cancel</x-button>--}}
                <x-button class="bg-gray-400 hover:bg-gray-500" {{-- x-on:click="show = false" --}}  wire:click="$set('showdeleteUserModal', false)">Cancel</x-button>
                <x-button type="submit" class="bg-blue-400 hover:bg-blue-500" wire:click="deleteUser">Continue</x-button>
            </x-slot>
        </x-confirmation-modal>
    </form>


</div>


{{--<form>--}}
{{--    <h1>Here we gooooo!!</h1>--}}

{{--    <table class="border">--}}
{{--        <tr>--}}
{{--            <th>Nada</th>--}}
{{--            <th>Contador</th>--}}
{{--            <th>Otro</th>--}}
{{--        </tr>--}}
{{--        <tr>--}}
{{--            <td>-</td>--}}
{{--            <td>1</td>--}}
{{--            <td>3</td>--}}
{{--        </tr>--}}
{{--        <tr>--}}
{{--            <td>-</td>--}}
{{--            <td>2</td>--}}
{{--            <td>4</td>--}}
{{--        </tr>--}}
{{--        <tr>--}}
{{--            <td colspan="3">--}}

{{--                    <x-button type="button" class="bg-red-400 hover:bg-red-500" wire:click="showModal">Delete</x-button>--}}
{{--                    --}}{{--                    <a href="#user-delete-modal" class="underline text-blue-500">Delete user</a>--}}
{{--            </td>--}}
{{--        </tr>--}}
{{--    </table>--}}

{{--@if($showModal)--}}
{{--    --}}{{--    <x-confirmation-modal title="title" body="bodyy" >--}}
{{--    <x-confirmation-modal wire:model.defer="showModal">--}}
{{--        <x-slot name="title">--}}
{{--            Are you really sure?--}}
{{--        </x-slot>--}}

{{--        <x-slot name="body">--}}
{{--            Please describe your question/issue in detail below:--}}
{{--        </x-slot>--}}

{{--        <x-slot name="footer">--}}
{{--            <x-button type="button" class="bg-gray-400 hover:bg-gray-500" x-on:click="show = false" --}}{{-- wire:click="hideModal" --}}{{-->Cancel</x-button>--}}
{{--            <a href="#" class="bg-gray-400 hover:bg-gray-500 text-xs uppercase py-2 px-4 rounded-md text-white transition-all duration-200">Cancel</a>--}}
{{--            --}}{{--            <x-button class="bg-gray-400 hover:bg-gray-500">Cancel</x-button>--}}
{{--            <x-button class="bg-blue-400 hover:bg-blue-500" wire:click="reportIssue">Continue</x-button>--}}
{{--        </x-slot>--}}
{{--    </x-confirmation-modal>--}}
{{--@endif--}}
{{--</form>--}}

