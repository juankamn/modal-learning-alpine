<x-modal name="contact-modal">
    <x-slot name="title">
        <h1 class="text-lg font-bold">Contact US</h1>
    </x-slot>

    <x-slot name="body">
        <p> Foobar </p>
    </x-slot>

    <x-slot name="footer">
        <x-button class="bg-blue-500 hover:bg-blue-600" @click="show = false">Close</x-button>
    </x-slot>
</x-modal>
