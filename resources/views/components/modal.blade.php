{{--<div  x-data="{ show : @entangle('showModal') }" x-show="show" style="display: none" @keydown.escape.window="show = false">--}}
@props(['name'])

<div id="{{ $name }}"
     x-data="{ show : false, name: '{{ $name }}' }"
     x-show="show"
{{--     x-on:modal-{{$name}}.window="show = true"--}}
     x-on:modal.window="show = ($event.detail === name)"
     style="display: none"
     @keydown.escape.window="show = false"
    {{ $attributes }}
>
    <div class="fixed inset-0 bg-gray-900 opacity-90" @click="show = false"></div>
    <div {{--@click.away="show = false"--}}
         x-show.transition="show"
         class="bg-white shadow-md max-w-sm h-48 m-auto rounded-md fixed inset-0"
    >
        <div class="flex flex-col h-full justify-between">
            <header class="p-6">
                <h3 class="font-bold text-lg">{{ $title }}</h3>
            </header>

            <main class="px-6 mb-4">
                {{ $body }}
            </main>

            <footer class="flex justify-end px-6 py-4 mt-6 bg-gray-200 rounded-b-md">
                {{ $footer }}
                {{--                <x-button class="bg-gray-400 hover:bg-gray-500">Cancel</x-button>--}}
                {{--                <x-button class="bg-blue-400 hover:bg-blue-500">Continue</x-button>--}}
                {{--                <button class="bg-gray-400 hover:bg-gray-500 text-xs uppercase py-2 px-4 rounded-md text-white transition-all duration-200">Cancel</button>--}}
                {{--                <button class="bg-blue-400 hover:bg-blue-500 text-xs uppercase py-2 px-4 rounded-md text-white transition-all duration-200">Continue</button>--}}
            </footer>
        </div>
    </div>
</div>
